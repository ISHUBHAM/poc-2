<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','HomeController@index');

Auth::routes();
Route::group(['middleware'=>['auth']],function() {
    Route::get('blogs','Blog\BlogController@index')->name('blog-list');
     Route::get('create-post','Blog\BlogController@createPost')->name('create-post');
    Route::post('upload','Blog\BlogController@upload')->name('upload');
    Route::post('storepost','Blog\BlogController@storePost')->name('storepost');
    Route::get('viewblog/{id}','Blog\BlogController@viewBlog')->name('view-blog');
    Route::get('edit/{id}','Blog\BlogController@editPost')->name('edit-post');
    Route::post('update/{id}','Blog\BlogController@updatePost')->name('update-post');
    Route::get('delete/{id}','Blog\BlogController@deletePost')->name('delete-post');
    Route::get('archieve/{id}','Blog\BlogController@archieve')->name('archieve-post');
    Route::get('archievelist','Blog\BlogController@archieveList')->name('archieve-post-list');
    Route::get('unarchieve/{id}','Blog\BlogController@unarchieve')->name('unarchieve');
    Route::get('profile','Blog\BlogController@profile')->name('profile');
    Route::post('profileupdate/{id}','Blog\BlogController@profileUpdate')->name('profile-update');
    Route::get('sortasc','Blog\BlogController@sortAsc')->name('sortasc');
});

