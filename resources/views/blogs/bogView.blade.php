@extends('layout.master');

@section('title')
    Create Post
@endsection
@section('content')

<div class="row mt-5">
	<div class="col-md-1">
		
	</div>
	<div class="col-md-7">
		{{$data->title}}
	</div>
	<div class="col-md-3">
		<p>Published on : {{$data->date}}</p>
	</div>
	<div class="col-md-1">
		
	</div>
</div>
<div class="row mt-3">
	<div class="col-md-1">
		
	</div>
	<div class="col-md-10">
		{!! $data->body !!}
	</div>
	<div class="col-md-1">
		
	</div>
</div>
@endsection